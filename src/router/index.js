import Vue from "vue";
import VueRouter from "vue-router";
import main from "../components/main";
import Readme from "../components/Readme";
import Downloads from "../components/Downloads";
import Charts from "../components/Charts3";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: main
  },
  {
    path: "/readme",
    name: "Readme",
    component: Readme
  },
  {
    path: "/downloads",
    name: "Downloads",
    component: Downloads
  },
  {
    path: "/charts",
    name: "Charts",
    component: Charts
  }
];

const DEFAULT_TITLE = 'hotmilk.ch';
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.afterEach((to) => {
  // Use next tick to handle router history correctly
  // see: https://github.com/vuejs/vue-router/issues/914#issuecomment-384477609
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
});

export default router;