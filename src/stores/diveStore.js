import { defineStore } from 'pinia'

export const useDiveStore = defineStore('computedDive', {
    state: () => {
        return {
            setpoint: 0,
            groundParameter: {
                groundTime: 40,
                groundDepth: 45,
            },
            ascendParameter: {
                toDeco: 0,
                toGasChange: 0,
                toSurface: 0,
            },
            diversAMS: [],
        }
    },
    actions: {
        setSetpoint(setpoint){
            this.setpoint = setpoint;
        },
        setGroundParameter(groundTime, groundDepth){
            this.groundParameter.groundTime = groundTime;
            this.groundParameter.groundDepth = groundDepth;
        },
        setAscendParameter(ascendToDeco, ascendToGasChange, ascendToSurface){
            this.ascendParameter.toDeco = ascendToDeco;
            this.ascendParameter.toGasChange = ascendToGasChange;
            this.ascendParameter.toSurface = ascendToSurface;
        },
        setDiversAMS(diversAMS){
            this.diversAMS = diversAMS;
        }
    },
    getters: {
        getSetpoint(state){
            return state.setpoint;
        },
        getGroundParameter(state){
            return state.groundParameter;
        },
        getAscendParameter(state){
            return state.ascendParameter;
        },
        getHighestAMS(state){
            console.log(Math.max(state.diversAMS));
            return Math.max(...state.diversAMS);
        }
    },
  })